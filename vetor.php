<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$despesas[0] = 345.55;
$despesas[1] = 135.00;
$despesas[2] = 600.00;
$despesas[3] = 900.00;
$despesas[4] = 400.00;

for ($i = 0; $i < 5; $i++){
    echo $despesas[$i] . "<br";
}

unset($despesas);//apaga/destroi o vetor

$despesas['mercado'] = 345.55;
$despesas['estacionamento'] = 135.00;
$despesas['alimentacao'] = 600.00;
$despesas['bar'] = 900.00;
$despesas['educacao'] = 400.00;

echo "<br> Despesas <br>";


foreach ( $despesas as $nome/*indice*/ => $gasto/*valor*/ ){
    echo "$nome: R$" . number_format( $gasto, 2, ',' , '.') . "<br>";
}
echo "<br><br>";


$diaSemana[0] = 'Segunda';
$diaSemana[1] = 'Terça';
$diaSemana[2] = 'Quarta';
$diaSemana[3] = 'Quinta';
$diaSemana[4] = 'Sexta';
$diaSemana[5] = 'Sabado';
$diaSemana[6] = 'Domingo';

$tarefa[0] = 'Trabalhar';
$tarefa[1] = 'Descansar';
$tarefa[2] = 'Viajar';
$tarefa[3] = 'Jogar Volei';
$tarefa[4] = 'Andar de bike';
$tarefa[5] = 'Estudar';
$tarefa[6] = 'Estudar mais ainda';

for($i = 0; $i < 7; $i++){
    echo "$diaSemana[$i] " . "$tarefa[$i] <br><br>";
}

