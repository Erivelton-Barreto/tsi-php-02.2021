<?php
//BÁSICO DO BÁSICO
//echo "Olá Mundo!";

//muito útil para DEBUG (NUNCA USAR EM PRODUÇÃO)
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*
Comentário 
em 
bloco
*/


$nome = 'Luiz Fernando'; //uma variavel em PHP

echo 'Olá, ' . $nome . '!<br><br>'; //aqui, foi concatenando

echo "Olá, $nome!<br><br>";

//CONDICIONAIS
if($nome == "Luiz Fernando"){
    echo "O nome é igual <br><br>";
}else{
    echo "O nome não é igual <br><br>";
}

$dia = 'sexta';
switch($dia){
    case 'segunda':
        echo 'Estude';
    break;

    case 'terça':
        echo 'Vá para aula de CMS';
    break;

    case 'quarta':
        echo 'va para aula de BD';
    break;

    case 'quinta':
        echo 'seja feliz aqui e agora';
    break;

    case 'sexta':
        echo 'Vá para Kombuca';
    break;

    default:
        echo 'Vá descansar';
}
echo '<br><br>';

$animal = 'cachorro';
$tipo = $animal == 'cachorro' ? 'mamifero' : 'desconhecido';

//confere se a variavel ja foi criada, se nao, informa a msg "não informado"
$sobrenome = $sobrenome_informado ?? 'não informado';

echo "<br> Sobrenome: $sobrenome<br>";

echo "<br> $animal é um animal do tipo: $tipo<br><br>";


//LOOPINS
echo 'FOR<br>';
for($i = 0; $i < 10; $i++){
    echo "Essa linha é $i <br>";
}
echo '<br><br>';

echo 'WHILE<br>';
$i = 0;
while($i < 5){
    echo "Essa linha é $i <br>";
    $i++;
}
echo '<br><br>';

echo 'DO WHILE<br>';
$i = 0;
do{
    echo "Essa linha é $i <br>";
    $i++;
}while($i < 3);
echo '<br><br>';

//COMO CHAMAR OUTROS CÓDIGOS
include 'link.html';//se não existir link.html dá um erro mas continua a execução
require 'link.html';//se não exixtir link.html, dá um erro fatal e para o programa
include_once 'link.html';//verifica se ja foi incluido antes, se sim, não inlui novamente
require_once 'link.html';//verifica se ja foi incluido antes, se sim, não inlui novamente
 